package homeWork13.classes;

import homeWork13.repo.CrudHuman;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class CrudHumanImpl implements CrudHuman {
    private static Connection dbconnection = null;
    PreparedStatement prSt = null;

    public CrudHumanImpl() {
        dbconnection = createConnection();
    }

    private Connection createConnection() {
        if (dbconnection == null) {
            Properties properties = new Properties();
            try {
                FileInputStream fileInputStream = new FileInputStream("src/main/resources/homeWork13/db13.properties");
                properties.load(fileInputStream);
            } catch (FileNotFoundException e) {
                System.out.println("Ошибка №1 при считывании настроек подключения к БД");
                e.printStackTrace();
            } catch (IOException ioException) {
                System.out.println("Ошибка №2 при считывании настроек подключения к БД");
                ioException.printStackTrace();
            }
            try {
                return DriverManager.getConnection(
                        properties.getProperty("db.url"),
                        properties.getProperty("db.user"),
                        properties.getProperty("db.password"));

            } catch (SQLException throwables) {
                System.out.println("Ошибка №3 при считывании настроек подключения к БД");
                throwables.printStackTrace();
            }

        }
        return dbconnection;
    }

    @Override
    public List<Human> getAllHumans() {
        List<Human> humanList = new ArrayList<>();
        ResultSet resSet;
        String selectString = "SELECT * FROM humans";
        try {
            prSt = dbconnection.prepareStatement(selectString);
            resSet = prSt.executeQuery();
            while (resSet.next()) {
                Human human = new Human(resSet.getInt("id"),
                        resSet.getString("name"),
                        resSet.getString("lastName"),
                        resSet.getString("patronymic"),
                        resSet.getString("city"),
                        resSet.getString("street"),
                        resSet.getString("house"),
                        resSet.getString("flat"),
                        resSet.getString("numberPassport"));
                humanList.add(human);
            }
        } catch (SQLException throwables) {
            System.out.println("Ошибка при занесении считывании всех Human");
            throwables.printStackTrace();
        }

        return humanList;
    }

    @Override
    public Human getHumanById(int id) {
        ResultSet resSet;
        String selectString = "SELECT * FROM humans WHERE id = " + id;
        Human human = null;
        try {
            prSt = dbconnection.prepareStatement(selectString);
            resSet = prSt.executeQuery();
            resSet.next();
            human = new Human(resSet.getInt("id"),
                    resSet.getString("name"),
                    resSet.getString("lastName"),
                    resSet.getString("patronymic"),
                    resSet.getString("city"),
                    resSet.getString("street"),
                    resSet.getString("house"),
                    resSet.getString("flat"),
                    resSet.getString("numberPassport"));
        } catch (SQLException throwables) {
            System.out.println("Human с таким id не существует");
            throwables.printStackTrace();
        }
        return human;
    }

    @Override
    public void createHuman(Human human) {
        String insertString = "INSERT INTO humans (name, " +
                "lastName," +
                "patronymic, " +
                "city, " +
                "street, " +
                "house, " +
                "flat, " +
                "numberPassport) " +
                "VALUES(?,?,?,?,?,?,?,?)";
        try {
            prSt = dbconnection.prepareStatement(insertString);
            prSt.setString(1, human.getName());
            prSt.setString(2, human.getLastName());
            prSt.setString(3, human.getPatronymic());
            prSt.setString(4, human.getCity());
            prSt.setString(5, human.getStreet());
            prSt.setString(6, human.getHouse());
            prSt.setString(7, human.getFlat());
            prSt.setString(8, human.getNumberPassport());

            prSt.executeUpdate();
        } catch (SQLException throwables) {
            System.out.println("Ошибка при создании Human");
            throwables.printStackTrace();
        }

    }

    @Override
    public void updateHuman(Human human) {
        Human oldHuman = getHumanById(human.getId());
        if (!human.equals(oldHuman)) {
            createHuman(human);
            deleteHuman(oldHuman);
        }
    }

    @Override
    public void deleteHuman(Human human) {
        String deleteString = "DELETE FROM humans WHERE id = " + human.getId();

        try {
            prSt = dbconnection.prepareStatement(deleteString);
            prSt.executeUpdate();
        } catch (SQLException throwables) {
            System.out.println("Ошибка при удалении Human");
            throwables.printStackTrace();
        }

    }
}
