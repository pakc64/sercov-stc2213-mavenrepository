package homeWork13;

import homeWork13.classes.CrudHumanImpl;
import homeWork13.classes.Human;
import homeWork13.repo.CrudHuman;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CrudHuman crudHuman = new CrudHumanImpl();
        List<Human> humanList = crudHuman.getAllHumans();
        System.out.println(humanList);
        System.out.println("");
        System.out.println(crudHuman.getHumanById(2));

        //Human human = crudHuman.getHumanById(6);
        Human newHuman = Human.of("Ефросий", "Зелепупкин", "Карыгандыевич",
                "Магадан", "Шестоковича", "3", "6", "53098720509");
        crudHuman.createHuman(newHuman);
//
//      crudHuman.deleteHuman(human);
//      human.setName("Mitrofan");
//      crudHuman.updateHuman(human);
    }
}
