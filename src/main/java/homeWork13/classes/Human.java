package homeWork13.classes;
import lombok.*;

@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class Human {

    private int id;
    @NonNull
    private String name;
    @NonNull
    private String lastName;
    @NonNull
    private String patronymic;
    @NonNull
    private String city;
    @NonNull
    private String street;
    @NonNull
    private String house;
    @NonNull
    private String flat;
    @NonNull
    private String numberPassport;

    @Override
    public String toString() {
        return "  id= " + id + " " +
                "  name= " + name + " " +
                "  lastName= " + lastName + " " +
                "  patronymic= " + patronymic + " " +
                "  city= " + city + " " +
                "  street= " + street + " " +
                "  house= " + house + " " +
                "  flat= " + flat + " " +
                "  numberPassport= " + numberPassport + "\n ";
    }

}