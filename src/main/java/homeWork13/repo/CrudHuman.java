package homeWork13.repo;

import homeWork13.classes.Human;

import java.util.List;

public interface CrudHuman {

    List getAllHumans();
    Human getHumanById(int id);
    void createHuman(Human human);
    void updateHuman(Human human);
    void deleteHuman(Human human);
}